import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../vuex/store'
Vue.use(VueRouter)

import slide from '../components/public/slide.vue'
import header from '../components/public/header.vue'
import home from '../components/page/home.vue'
import teacher from '../components/page/teacher.vue'
import attendance from '../components/page/attendance.vue'
import exam_time from '../components/page/exam_time.vue'
import login from '../components/page/login.vue'

const router = new VueRouter({
	mode : 'hash',
	base : __dirname,
	routes : [
		{
			path : '/',
			components : {
				'slide' : slide,
				'header' : header,
				'default' : home
			}
		},
		{
			path : '/teacher',
			components : {
				'slide' : slide,
				'header' : header,
				'default' : teacher
			}
		},
		{
			path : '/attendance',
			components : {
				'slide' : slide,
				'header' : header,
				'default' : attendance
			}
		},
		{
			path : '/exam_time',
			components : {
				'slide' : slide,
				'header' : header,
				'default' : exam_time
			}
		},
		{
			path : '/login',
			components : {
			
				'default' : login
			}
		}
	]
})
//每次进入新组件后的钩子
router.afterEach(function(to){
	//判断是否登录
	if(store.state.isLogined){
		//一个请求判断session是否存在，不存在就跳转到 /login
		
	} else {
		router.push('/login')
	}
	//每次组件滚动条回到顶部
	window.scrollTo(0,0)
});

export default router;
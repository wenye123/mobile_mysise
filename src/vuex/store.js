import Vue  from 'vue';
import Vuex from 'vuex';
import { Indicator } from 'mint-ui';
import VueResource from 'vue-resource'

Vue.use(VueResource)
Vue.use(Vuex)

export default new Vuex.Store({
	state : {
		//是否登录状态
		isLogined : false,
		//用户学号和密码
		user : "",
		pass : "",
		//模拟登录的cookie
		sise_cookie : '',

		//是否显示侧栏
		slideShow : false,

		//导航栏标题
		title : "首页",

		//动画
		animateIn : 'slideInRight',
		animateOut : 'slideOutLeft',
		animateMode : '',

		//需要刷新的组件名字
		refreshName : '',

	},
	mutations : {
		// 改变登录状态为真
		setLoginedFn(state,payload){
			state.isLogined = payload.status;	
		},
		//更改用户学号和密码
		setUserFn(state,payload){
			state.user = payload.user;	
		},
		setPassFn(state,payload){
			state.pass = payload.pass;	
		},
		//更改模拟登录的cookie
		setSiseCookieFn(state,payload){
			state.sise_cookie = payload.sise_cookie;	
		},
		// 改变导航栏标题
		changeTitleFn(state,payload){
			//console.log("改变标题")
			state.title = payload.title;	
		},
		//显示侧栏
		showSlideFn(state,payload){
			state.slideShow = true;			
		},
		//关闭侧栏
		closeSlideFn(state,payload){
			state.slideShow = false;			
		},
		//改变动画
		changeAnimate(state,payload){
			if(payload.mode == 'normal'){
				state.animateIn = 'slideInRight'
				state.animateOut = 'slideOutLeft'
				state.animateMode = ''
			} else if(payload.mode == 'reverse'){
				state.animateIn = 'slideInLeft'
				state.animateOut = 'slideOutRight'
				state.animateMode = ''
			} else if(payload.mode == 'change'){
				state.animateIn = payload.animateIn
				state.animateOut = payload.animateOut
				state.animateMode = payload.animateMode
			}
		},
		//改变需要刷新的组件
		changeRefreshName(state,payload){
			console.log('执行到了mutations')
			state.refreshName = payload.refreshName
		},
	},
	actions: {
		//get获取数据
		getData(context,payload){
			Vue.http.get(payload.url,{
				timeout : 7000,
				before : function(){
					//打开加载数据框
					Indicator.open({
						text : '加载中....',
						spinnerType : 'triple-bounce'
					});
				}
			}).then(function(response){
				// 判断是不是json对象
				 var data = typeof response.body =='object'?response.body:JSON.parse(response.body);
				//关闭加载数据框
				Indicator.close();
				//执行回调函数
				payload.callBack(data)	
			},function(response){
				console.log(response);
			}).catch(function(msg){
				console.log('请求程序出错：'+msg);
			});
		},
		// post提交数据
		postData(context, payload) {
			Vue.http.post( payload.url, payload.data, {
				timeout : 20000,
				before : function(){
					Indicator.open({
						text : '加载中....',
						spinnerType : 'triple-bounce'
					});
				},
				//设置为表单提交的方式，不然没效果
				emulateJSON : true
			}).then(function(response){
				//console.log(response)
				var data = typeof response.body =='object'?response.body:JSON.parse(response.body);
				Indicator.close();
				payload.callBack(data)	
			},function(response){
				var data = {
					"code" : 404,
					"msg" : "failed",
					"resData" : []
				}
				Indicator.close();
				payload.callBack(data)
			}).catch(function(msg){
				console.log('请求程序出错：'+msg);
			});
		}
	}
})
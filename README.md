# 移动misise

> 使用vue2.0 + php(模拟登录)搭建

## 功能
1. 查看课程表和学生基本信息
2. 获取指定老师的电话号码和工号
3. 查询成绩
4. 查看考勤
5. 查看考试时间

## 项目展示
[https://git.oschina.net/wenye123/demo_mysise](https://git.oschina.net/wenye123/demo_mysise)

## Start

 - 克隆或者下载这个项目
 - 进入文件夹，安装依赖

``` bash
npm install
```

## Develop

``` bash
# 运行在localhost:8080
npm run dev
```

## Build

``` bash
# 打包项目
npm run build
```
